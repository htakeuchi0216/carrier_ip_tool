package main
import (
     "strings"
     "fmt"
     "github.com/PuerkitoBio/goquery"
     "regexp"
)

func GetPageDocomo(url string) {
    fmt.Println(" === Docomo === ")
     doc, _ := goquery.NewDocument(url)
     doc.Find(".txt.delmb").Each(func(_ int, s *goquery.Selection) {
         arr0 := strings.Split( s.Text() , "\n")
         for _, v := range arr0 {
             if v == "" { 
                 continue
             }
             fmt.Print(fmt.Sprintf("%s\n",  v))
          }
     })
}

func GetPageSB(url string){
    fmt.Println(" === SB === ")
     doc, _ := goquery.NewDocument(url)
     doc.Find("tr>.txt-m").Each(func(_ int, s *goquery.Selection) {
         text := s.Text()
         if m, _ := regexp.MatchString("^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}/[0-9]{1,2}", text); m {
             fmt.Println( text )
         }
     })
}


func GetPageAu(url string){
    fmt.Println(" === AU === ")
     doc, _ := goquery.NewDocument(url)
     doc.Find("#primaryArea>dl>dd").Each(func(_ int, s *goquery.Selection) {
         text := s.Text()
         fmt.Println( text )
     })
}

func main() {
     url := "https://www.nttdocomo.co.jp/service/developer/smart_phone/spmode/index.html"
//     GetPageDocomo(url)
//     url = "https://www.support.softbankmobile.co.jp/partner/home_tech1/index.cfm"
//     GetPageSB(url)
     url = "http://www.au.kddi.com/developer/android/kaihatsu/network/"
     GetPageAu(url)

}